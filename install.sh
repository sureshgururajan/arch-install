# https://gitlab.com/-/snippets/2151004

ping google.com

# Set clock
timedatectl set-ntp true

# Check volumes
fdisk -l

# Partition volume
fdisk /dev/sda

# Use the helper commands to create the following partitions
# sda1:
# - Type: EFI system
# - Start: 2048
# - End: +550M

# sda2:
# - Type: Linux Root x86_64
# - Start: default
# - End: remainder of the disk

# Boot partition (EFI system type) must be FAT32
mkfs.fat -F32 /dev/sda1

# Using EXT4 for the root linux partition
mkfs.ext4 /dev/sda2

# Mount the sda2 partition
mount /dev/sda2 /mnt

# Install essential packages
pacstrap /mnt base linux linux-firmware
# takes a couple minutes to install

# Configure the system
# This command defines how disk partitions, various other block devices, or remote filesystems should be mounted into the filesystem
genfstab -U /mnt >> /mnt/etc/fstab


# Change to root 
arch-chroot /mnt


# Set the timezone
ln -sf /usr/share/zoneinfo/Region/City /etc/localtime

# Set the hardware clock
hwclock --systohc

# Install nano editor
pacman -S nano

# Edit the locale.gen file to set localization
nano /etc/locale.gen

# Uncomment en_US UTF UTF8
# Now generate locale
locale-gen

# Now, set the hostname for the machine
nano /etc/hostname
# and enter a hostname
# For me it's sureshgarchinstall

nano /etc/hosts
# and add these values
# 127.0.0.1	localhost
# ::1		localhost
# 127.0.1.1	sureshgarchinstall.localdomain	sureshgarchinstall


# set a root password
passwd


# Create an additional sudoer (recommended)
# DO NOT use root for anything
# Create a root user password and never use the root user again unless absolutely necessary
# See https://www.youtube.com/watch?v=PQgyW10xD8s&t=489s

useradd -m sureshg # (make another user)
passwd sureshg # (set that user's password)
usermod -aG wheel,audio,video,optical,storage sureshg

# Now install sudo and add sureshg to the sudoers list
pacman -S sudo
EDITOR=nano visudo
# Then uncomment %wheel ALL=(ALL) ALL


# Install a capable bootloader
# We're installing grub here
pacman -S grub efibootmgr

# Refer: https://wiki.archlinux.org/title/EFI_system_partition#Mount_the_partition
# https://en.wikipedia.org/wiki/EFI_system_partition
# could be any mount directory as far as my current understanding goes
mkdir /boot/EFI

# Mount EFI partition to /boot/EFI
mount /dev/sda1 /boot/EFI

# Do a grub install
grub-install --target=x86_64-efi --bootloader-id=GRUB

# Create the grub config
# with boot menu entries
grub-mkconfig -o /boot/grub/grub.cfg

# Install completed at this point.
# The next steps are what I found in 
# https://www.youtube.com/watch?v=PQgyW10xD8s&t=489s

pacman -S networkmanager
systemctl enable NetworkManager


# now exit chroot and reboot system
exit
umount /mnt
shutdown now

# Post-install steps
# For virtualbox: https://wiki.archlinux.org/title/VirtualBox/Install_Arch_Linux_as_a_guest

sudo pacman -S virtualbox-guest-utils

